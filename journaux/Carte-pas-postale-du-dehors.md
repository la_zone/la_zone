---
description: Petite carte pas postale de pntbr de scopyleft - sur ses activités liées à la Zone (celle du dehors)
---


## Jeudi 14 février 2019

### Journée étape _ (milieu de mandat)

On a passé trois factures sur -Chorus-Pro- trop content. J'ai appris pleins de trucs sur le chemin.  
Par exemple, que plus rien n'empêchait -scopyleft- de faire des cessions Dailly,  
que sans trop faire suer Héla et Marielle - (je continue à essayer de vérifier ça) - on pouvait obtenir des -services faits- validés en quelques jours (3 ou 4 ?).  
Qu'on pouvait facturer 1k /jr et gagner en autonomie.  
Qu'un début de solution émergeait pour pouvoir payer les sous-traitant.e.s 30 jours après leur premier jour de travail sans que ça grince côté trésorerie. 
  
Pour la suite, je me suis donné jusqu'à fin mars pour voir si dans ce contexte j'avais de l'enthousiasme ou si j'allais juste accompagné "froidement".  
  
Les sujets en chantier et en brouillons sur lesquels on va peut-être bossé :  
- démarrer une ligne Dailly et documenter  
- accordage (ou pas) avec Ishan pour commencer : https://hackmd.io/1JkBZKyDQ1yLlLo_BQMYdg#  
- afiner le trésotonome - ce truc qui génère le budget furtif des startups
- fureteur et ("infusion" Pierre de Maulmont) sur dossierSCO  
- clause sociale comme outil de disparité    
- proposer à Héla et Marielle (par exemple) de simplifier des trucs chiants qu'elles se tapent   
- proposer de l'aide aux personnes qui souhaitent rentrer en contact avec -Chorus- et documenter  

## Mardi 16 octobre 2018

Le moral est 🌤 

On s'est accordé avec Nicolas -CeL- et le dépôt réponse est ouvert au public, ça me fait plaisir et ça va être plus pratique pour discuter avec l'extérieur (Aliocha par exemple). 

Raphaël /ut7 a dégagé un peu de temps pour que l'on papaute demain avec Sabine. L'idée est de pouvoir préparer quelques sujets avant de débarquer au séminaire de l'incubateur.   

On a commencé à échanger avec Pascal R. sur la légalité de facturer l'Unité d'Œuvre avec un Taux Journalier différent de notre réponse à l'Appel d'Offre. Ça première impression semble assez claire, il devrait faire une réponse rapidement.

Pour clôturer la journée je viens d'ajouter un thème [!daktary](https://github.com/daktary-team/) > [https://la_zone.gitlab.io/la_zone/](https://la_zone.gitlab.io/la_zone/) pour consulter ce dépôt.

## Lundi 15 octobre 2018

Le moral est 🌤  

Par ·| en direct de la Zone du dehors.   

Aujourd'hui, nous avons re-re-re-discuté des prix, plus exactement du Taux Journalier.  
L'occasion de jouer les archéologues en partant explorer le grenier de la vieille réponse.  
Un peu de gDrive de 18_bam_18, de fil d'issue et de pioche dans les souvenirs.  

Avec le [BPU](https://gitlab.com/la_zone/18_bam_181/blob/master/reponse/BPU.xlsx) je me suis tatoué *60 jrs = 60k€* pour tous les métiers.

J'ai relu le [mémoire technique](https://gitlab.com/la_zone/18_bam_181/blob/master/reponse/M%C3%A9moire%20technique.pdf) :  
> Nous avons pour habitude, dans nos structures respectives, de ne pas faire de différence de valeur dans le travail de chacun. Aussi, proposer une grille de tarif où toutes les UO sont valorisées de la même manière nous a paru évident
> Le montant décidé par le collectif couvre beaucoup plus que notre intervention. Il couvre :
> * le travail administratif nécessaire pour que toutes et tous puissent travailler avec la DINSIC dans de bonnes conditions ;
> * une provision pour l’organisation de rétrospectives du collectif, des personnes en sous-traitance via le collectif et des membres de la DINSIC qui souhaitent participer ;
> * nos actions sociales tel que décrites dans la note sur la clause d’inclusion (voir ci-dessus) ;
> * une marge de manœuvre pour improviser des actions « correctives » en cas de difficulté ;
> * des actions de transmission concernant les produits de la DINSIC et les outils mis en place dans le cadre de nos missions, et
29
> * toutes les activités qui permettent au collectif de créer plus de valeur pour les communs et les utilisatrices et utilisateurs.

Je crois que je suis plutôt -ok- avec ça. Deux trois trucs qui grattent mais pas vraiment urticant.

Avec [fil d'issues](https://gitlab.com/la_zone/18_bam_181/issues/12), je me suis rendu compte que je restais aligné sur :

> En réflexe, mon hypothèse serait de construire la proposition à partir de la réponse à cette question :
> Quel prix journalier limite (prix de friction) pourrait être accepté par le jury ?
> Si le prix dépasse notre tarif habituel ou celui du sous-traitant, ça pourrait permettre de construire les propositions avec du mou. 
> Par exemple, j'imagine que l'on puisse s'en servir pour improviser des trucs quand la mission dérive, et aussi pour essayer "d'augmenter la valeur" quand on découvre de nouvelles choses en cours de mission.
> Quelques pistes improvisées :
> * Convertir vers les communs - 🌱
> * Improviser des actions "correctives" en milieu de mission difficile - 🧘
> * Transmettre culture et pratiques aux parties prenantes - 🐾
> * Essayer et explorer des formes de travail collaboratif : Mob, walkingDev, Infusion - 🧗
> * Pour faire intervenir des invité·e·s : WorryDream, CNV - ⛄️
> * etc.


Il était long ce -fil- ça m'a donné envie de ne pas le redémarrer, on a discuté avec l'équipe de scopyleft puis avec Mathieu Agopian qui va démarrer sur la startup [classe à 12](https://gitlab.com/la_zone/incubateur_dinsic/tree/master/classe-a-12) et on a décidé de prévenir Rapahël que l'on partait sur l'option de 1000 € /jr.

Demain, je pense bosser avec Pascal Romain pour vérifier la légalité de facturer un autre montant que celui du BPU que nous avons indiqué dans la réponse à l'appel d'offre.

Cette Première carte ressemble à un truc tout décousu, et elle va rester en l'état parce que je vais partir me coucher.


