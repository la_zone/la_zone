# 2018-10-01

Rangement des projets du groupe La Zone sur gitlab:
* `18_bam_181` est gardé pour archive (le repo est privé, les issues sont publiques)
* `incubateur_dinsic` est passé en confidentiel
* `valeurs` et `ao-sans-tailleur-ni-cravate` sont publics

Nouveau projet: `La Zone`, public, qui contient les discussions générales sur le groupement.
Le README (https://gitlab.com/la_zone/la_zone/blob/master/README.md) doit être le point d’entrée quand on cherche une info. Il n’y a ni wiki, ni issues sur le projet, tout va dans le repo, qui ne sert de toute façon qu’à documenter.

Un peu de documentation sur le projet `incubateur_dinsic` qui sert à suivre les relations avec la DINSIC. On pourrait aussi le renommer “confidentiel” si on a d’autres sujets à garder privés. Pas de wiki non plus sur ce projet qui est lui-même documentaire: autant tout mettre dans le repo. Ajout au README d’une ébauche de procédure sur le suivi d’une commande et d’un label “expression de besoin” pour les issues.