Les informations ici sont recueillies au fil de l’eau, peuvent être fortement incertaines, changeantes, en cours de réflexion. Il ne s’agit pas d’une base de vérité, mais pour montrer les réflexions en cours.

**Mardi 2 octobre**

Rencontre avec avec l’EPEC pour discuter de la clause d’insertion sociale.

Il faut faire travailler une personne en insertion 1h par 2000€ de facturation. En embauche directe, achat d’heures auprès d’une structure ou en sous-traitance.

Ils doivent être liées à l’objet du marché, mais ça peut être du support.

**Lundi 1er octobre**

Journée entière dans les locaux de /ut7 pour discuter sur La Zone en général.

Ça a permis de mieux définir ce qu’on veut faire de La Zone : en particulier ça reste une forêt peu définie, dont un des arbres est le marché avec la DINSIC.

**Mercredi 26 septembre**

* J’ai assisté aux discussions au sujet du contrat de sous-traitance de Octo pour voir si ça nous concerne. Je pense qu’il y a une tendance de juriste à vouloir rappeler la loi dans le contrat et je ne suis toujours pas convaincu que ça nous apporte grand chose
* Il semblerait que pour la startup _preuve de covoiturage_ on se dirige vers un portage par La Zone

**Mercredi 19 septembre**

* Je suis passé au bureau de G. (comptabilité DINSIC) pour demander comment faire pour que l’argent puisse arriver directement sur le compte d’un co-traitant. Elle va se renseigner, il faudra probablement un document supplémentaire.
* Au petit séminaire de l’incubateur, a été demandé quelles seraient les doléances des prestataires pour que ça se passe au mieux
  * Ne pas imposer d’assurance
  * Pas de contrat (ou alors qui ne soit pas un abus comme celui proposé il y a peu par le précédent attributaire)
* Discussions avec une personne de /ut7 (qui a proposé l’idée de ce journal) pour mieux voir nos attentes et la situation présente face à ce ressenti. Des échanges il en est ressorti des points de vigilance qui peuvent être source d’incompréhension et donc de tensions :
  * Paris vs. non-parigot
  * A déjà travaillé ou pas pour beta.gouv.fr

**Mardi 18 septembre**

* Les gens de data.gouv.fr (2 dev + 1 dev de Codeurs en Liberté) et de api-entreprise (3 dev) ont signalé qu’ils souhaiteraient venir avec nous
  * Pour api-entreprise une baisse de budget est prévue, et moins de marge permettra donc de limiter la casse
  * Une des personnes a signalé que le contrat pouvait être utile vis-à-vis de sa banque
  * Certains sont prêts à négocier les délais de paiements si ça peut nous aider pour la trésorerie.