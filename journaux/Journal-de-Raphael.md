# 1er octobre, c'est la zone à /ut7

Je note mes activités du matin dans un journal.

## Où documenter ?

Je commence mon activité autour de la question "où documenter",
en créant mon journal, ici. 

TODO: Quand je saurais où publier cet information, la copier à l'endroit qui convient.

Karine et Pierre causent un peu avec moi.

Je notes mes réflexions.

Y'a des envies de documenter pour :

- partager de l'information pour les besoins opérationnels des membres du groupement
- créer des ressources communes pour l'extérieur

Je recense les endroits où on pourrait documenter :

- [ce wiki, publié sur gitlab](https://gitlab.com/la_zone/18_bam_181/wikis/home)
- [le wiki de betagouv](https://github.com/betagouv/beta.gouv.fr/wiki)
- [les issues du projet 18_bam_181 de la_zone sur gitlab](https://gitlab.com/la_zone/18_bam_181/issues)
- des espaces privés (i.e. non publics) de partages de documents

J'aimerai avoir une idée claire de où déposer quelle information, en fonction de la nature de l'information.

Plus tard dans la matinée, je découvre que David vient de créer [un repo pour stocker des restitutions de nos rencontres en présentiel](https://gitlab.com/la_zone/presentiels)

Plus tard dans la journée, Nicolas fait le tri dans les repo de la zone sur gitlab.

## Doit-on s'aligner ?

Je suis seul à cette session. Ça tombe bien, j'ai un journal.

J'aime bien la question : "doit-on s'aligner ?", car, bien que je ne l'ai pas
entendue posée, j'anticipe que plusieurs personnes
présentes ont une réponse en tête, et qu'elle ne soit pas identifique pour
tou·te·s.

Quand j'y réfléchis, je réalise qu'un meilleure question serait : "sur quoi
convenons-nous qu'il est nécessaire de s'aligner", et "sur quoi
convenons-nous qu'il n'est pas nécessaire de s'aligner ?".
Et enfin "Est-ce grave si nous ne trouvons pas d'accord sur les deux questions
précédentes ?"

## Comment détecter les opportunités d'équipes

Je suis seul à cette session.
Voilà qui va me permettre de poser clairement la problématique :

Aujourd'hui, j'observe qu'on reçoit des demandes dans le cadre du marché
`18_bam_181`, et que chaque structure membre du groupement y répond
individuellement.

Ce mode de fonctionnement ne permettrait pas de répondre positivement à une
demande où aucune structure n'aurait à elle seule les ressources pour répondre,
mais où un collectif de plusieurs personnes physiques, issues de plusieurs
structures auraient l'envie et les moyens de répondre ensemble, (et ce avec éventuellement
d'autres personnes qu'elles voudraient impliquer).

Comment faire pour que ça puisse arriver ?

# 31 octobre 2018

## rendez-vous avec Géraldine

Avec Tristram, on va voir Géraldine, pour répondre aux questions notées lundi
dernier avec Nicolas (dans le journal des plénières).

Concernant ces questions, on apprend que :

- le bon de commande est envoyé au co-traitant identifié dans le devis ;
- c'est ce même co-traitant qui recevra l'avance de 5% ;
- le paiement est envoyé à celui qui émet la facture ;
- ça simplifie les contrôles si c'est la même entité qui présente le devis et la facture, mais ce n'est pas indispensable.

Ainsi, si /ut7 était destinataires de certaines avances qui auraient dues êtres destinées à d'autres, c'est parceque nos devis sont mal présentés
(par exemple, sur un devis émis par CeL, on affichait "la zone", "/ut7", et en petit CeL)

Ce qu'il faut retenir : **Sur un devis, afficher clairement le nom et l'adresse du co-traitant.**
Par exemple, si l'émetteur du devis est CeL, l'afficher clairement sur le devis,
plutôt que d'afficher "la zone" + "/ut7" et en petit CeL.

Une fois reçu, le devis "passe à la validation", et c'est le pôle de Géraldine
qui reçoit le devis, émet une demande d'achat, puis le centre de service
partagé de la DSAF (hors dinsic) émet le bon de commande.

Puis, quand on veut facturer, il faut produire un livrable qui permettra à l'incubateur
de fournir procés verbal de prestation partielle au même service.

Attention : à partir du 7 décembre, c'est la cloture budgetaire de fin de gestion.
Tous les services fait émis après cette date "consommeront des CPs"" (pas compris).
donc de pas trainer à traiter tous les trucs à facturer fin novembre.

Ensuite, mettre la facture dans chorus pro, et s'assurer que le PV est validé par Hela

Est-ce que c'est nécessaire de mettre une référence au PV dans la facture ? Géraldine ne sait pas,
elle suggère de demander à François.

## on enchaine en passant dans le bureau de François

C'est Francois qui gère l'opérationel.

C'est lui qui émet les demandes poour transformer en commande.

Il vérifie que le dévis est conforme au marché, au prix conforme au marché,
notamment aux BPU.

Je comprend que dans Chorus pro, il doit y avoir une commande pour chaque
fournisseur.  Et le CCAP dira si on est contraint de déclarer nos
sous-traitants.

# 7 janvier 2018

À la boutique /ut7.

## suivi des factures

Fin décembre, j'ai été surpris de pas recevoir la facture d'une personne qui
bosse pour Zam, alors qu'elle pensait que je l'avais sous la main.
Après explication, j'apprend qu'elle avait déposé la facture dans la base
partagée "Suivi" de Airtable - outil que je n'ai pas l'habitude d'utiliser.
Il y a eu un mismatch entre nos attentes, et notre (mé)connaissance de la
base Suivi en est à l'origine. Un mystère à éclaircir.

Aujourd'hui, j'enquête pour comprendre à quoi sert la base "Suivi".  Et je note
ce que je comprend dans l'espoir de donner du corps à ma stratégie : laisser
les choses se faire sans tenter de les coordonner, se donner le temps de les
observer, documenter ce qui émerge, et si besoin, réfléchir à l'améliorer.

Je comprend que la vocation première de cette base, c'est d'aider à calculer le
solde non facturé d'un bon de commande.  Elle a été crée par des membres de
CeL, et mise à disposition des attributaires de la zone.

Je sais que cet outil n'est pas utilisé par toustes (c'est le cas pour Zam),
ce n'est peut-être pas un problème, mais j'ai envie d'y réfléchir…

Il y a plusieurs contextes dans lesquels on peut vouloir faire le suivi des
bons de commandes et des factures. Je compte 4 contextes : l'incubateur, le
groupement, le coattributaire, la startup. Chaque contexte a ses propres
contraintes (quelles données, accessibles par qui, et comment ?), qui peut
demander une complexité spécifique, et cumuler la complexité de tous les
contextes, ça peut faire trop. Et en même temps, avoir des données
centralisées, ça peut avoir un intérêt - qui reste à évaluer.

Côté Zam, j'ai déjà fait un tableau de suivi spéficique pour l'équipe
(contexte : startup) qui mériterait d'être améliorié, et qui fait déjà doublon
avec le SI comptable de /ut7 (contexte : coattributaire).

Plus tard dans la journée, sur slack, Nicolas détecte que l'usage de base Suivi
n'est pas le même entre Compétence pro (réalisé par /ut7) et les d'autres
startups (porté ou réalisé par scopyleft).

Si je comprends bien, pour certaines lignes, la colonne "consommé" contient un
montant à facturer à l'état, et dans d'autres cas, elle contient un montant
facturé par les sous-traitants. 

Je me renseigne auprès d'Étienne au sujet de Compétence pro. Effectivement, il
utilise la base suivi, et ce uniquement dans le but de calculer le montant
restant à facturer, à chaque fin de mois.

Enfin, comme le note Tristram, la base est conçue pour suivre la consommation
totale, mais ne permet pas de faire le suivi de facturation partielle.

Côté Zam, l'usage du notre tableau de suivi sur Zam est le suivant :
- savoir combien il reste à facturer sur un bon de commande par le cotraitant
  (on fait des factures partielles)
- suivre les factures du cotraitant ET de chacun des sous-traitants
- suivre le montant prélevé par /ut7 pour charge administrative (ce que je ne
  compte pas en jour)

Par ailleurs, ce tableau est intégré dans une feuille utilisée par toute l'équipe Zam.

Je cause avec Stéphane pour parler des habitudes de scopyleft.  scopyleft
n'utilise pas airtable, et l'usage d'un document de suivi ne s'est pas encore
imposé avec classe à 12. Ça va venir avec DossierSco, son intention est
d'utiliser un fichier plat en markdown.

Côté DTC, rien à facturer pour l'instant, donc il n'y a pas encore de
facturation à suivre.

Sans surprise, tout ça est donc très hétérogène, et je ne détecte pas encore le problème
que cela pourrait créer. Dans l'immédiat je vais confirmer à Maïtané qu'elle peut
ignorer la base Suivi.
