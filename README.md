## [La Zone](https://gitlab.com/la_zone/la_zone) (ce projet, public)

Présentation, notes et discussion sur le groupement.
* [Suivi des budgets : suivi.la.zone](http://suivi.la.zone)
* [Groupe slack : discussion.la.zone](http://discussion.la.zone)
* [Notes et journaux des membres](https://la_zone.gitlab.io/la_zone/)

## [incubateur_dinsic](https://gitlab.com/la_zone/incubateur_dinsic) (confidentiel)

Le suivi des relations avec la DINSIC. Par ailleurs, la DINSIC peut contacter les membres du groupement à l’adresse relations-dinsic@la.zone.

## [18_bam_181](https://gitlab.com/la_zone/18_bam_181) (archive)

La réponse à l’appel d’offre de la DINSIC.
* issues (publiques) et repository (confidentiel)
* Le dépôt est en privé parce que <à compléter>  

## [ao-sans-tailleur-ni-cravate](https://gitlab.com/la_zone/ao-sans-tailleur-ni-cravate) (public)

[Le dépôt a été déplacé](https://gitlab.com/pntbr/ao-sans-tailleur-ni-cravate)  
Tentative pour répondre à la question : Comment pourrait-on répondre naturellement (sans séduction) à des appels d'offre


## [valeurs](https://gitlab.com/la_zone/valeurs) (public)

Discussions sur les valeurs communes, l’éthique du groupement. [site pour la consultation](https://la_zone.gitlab.io/valeurs/)


